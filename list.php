<?php


include 'vendor/autoload.php';
$users = (new Classes\User())->getAll();
?>

<html>
<head>
    <title>User Crud</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <h1>User List</h1>
        <a href="index.php" class="btn btn-success">Add User</a>
        <table class="table">
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
            </tr>

            <?php foreach ($users as $user): ?>
                <tr>
                    <td><?php echo $user->getFirstName(); ?></td>
                    <td><?php echo $user->getLastName(); ?></td>
                    <td><?php echo $user->getEmail(); $id=$user->getID();?></td>
                    <td><a href="edit.php?id=<?=$id?>" title="Edit" alt="Edit" class="btn btn-default">Edit</a></td>
                    <td><a href="delete.php?id=<?=$id?>" title="Delete" alt="Delete" class="btn btn-danger">Delete</a></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>
</body>
</html>
