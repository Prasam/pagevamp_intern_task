<?php


include 'vendor/autoload.php';


$ID = $_GET['id'];

$users = (new Classes\User());
$users->setID($ID);
$users->getAllByID();
$email = $users->getEmail();
$firstName = $users->getFirstName();
$lastName = $users->getLastName();
?>

<html>
<head>
    <title>User Crud</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <h1>Edit Users</h1>
        <form action="update.php" class="form" role="form" method="post">
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" name="email" value="<?php echo $email; ?>"/>
                <input type="hidden"   name="ID" value="<?php echo $ID; ?>"/>
            </div>
            <div class="form-group">
                <label for="first_name">First Name</label>
                <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $firstName; ?>"/>
            </div>
            <div class="form-group">
                <label for="last_name">Last Name</label>
                <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $lastName; ?>">
            </div>
            <div class="form-group">
                <button class="btn btn-success">Update</button>
            </div>
        </form>
    </div>
</div>
</body>
</html>
