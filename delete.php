<?php

include 'vendor/autoload.php';

$ID = $_GET['id'];

$user = new \Classes\User();
$user->setID($ID);
if ($user->delete()) {
    header('location:list.php');
}
